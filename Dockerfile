FROM golang:1.15-alpine AS build

WORKDIR /app
ENV GOFLAGS="-mod=readonly" \
    GOPRIVATE="gitlab.com/trilliot/*"

# Prepare .netrc for private repositories
ARG GITLAB_USER
ARG GITLAB_PASS
RUN echo "machine gitlab.com login $GITLAB_USER password $GITLAB_PASS" >> ~/.netrc

# Get dependencies
COPY go.mod go.sum ./
RUN go mod download && go mod verify

# Build app
COPY . .
RUN CGO_ENABLED=0 go build -installsuffix "static" ./cmd/hello

# Create security user
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/app" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "10001" \
    "appuser"

# ---

FROM scratch

COPY --from=build /etc/passwd /etc/group /etc/
COPY --from=build /app/hello /app/hello

USER appuser:appuser

ENTRYPOINT [ "/app/hello" ]
