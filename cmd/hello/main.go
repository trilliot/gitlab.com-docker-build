package main

import (
	"fmt"

	"github.com/namsral/flag"
)

var name = flag.String("name", "", "name to display")

func sayHello(name string) string {
	if name == "" {
		return "Hello, world!"
	}
	return fmt.Sprintf("Hello, %s!", name)
}

func main() {
	flag.Parse()

	greet := sayHello(*name)
	fmt.Println(greet)
}
