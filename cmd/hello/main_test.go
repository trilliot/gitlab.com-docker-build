package main

import "testing"

func TestSayHello(t *testing.T) {
	tt := []struct {
		name string
		in   string
		out  string
	}{
		{"default", "", "Hello, world!"},
		{"named", "Teddy", "Hello, Teddy!"},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			out := sayHello(tc.in)
			if out != tc.out {
				t.Errorf("expected output %q; got %q", tc.out, out)
			}
		})
	}
}
